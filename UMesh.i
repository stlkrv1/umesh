%module UMesh

%include <exception.i>

%exception {
  try {
    $action
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}

%feature("autodoc","3");

%include "std_string.i"
%include "std_vector.i"
%include "std_array.i"
%include "stdint.i"
%include "std_deque.i"
%include "std_list.i"
%include "std_map.i"
%include "std_unordered_map.i"
%include "std_pair.i"
%include "std_set.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_array.i"
%include "std_shared_ptr.i"

%{
#include "./UMesh.h"
using namespace UMeshLoader;
%}

%shared_ptr(UMeshLoader::UMesh)
%shared_ptr(UMeshLoader::Attribute)

namespace std {
   %template(Float2Array) array<float, 2>;
   %template(Float3Array) array<float, 3>;
   %template(Float4Array) array<float, 4>;

   %template(UInt2Array) array<uint32_t, 2>;
   %template(UInt3Array) array<uint32_t, 3>;
   %template(UInt4Array) array<uint32_t, 4>;

   %template(UIntVector) vector<uint32_t>;
   %template(UInt2Vector) vector<array<uint32_t, 2>>;
   %template(UInt3Vector) vector<array<uint32_t, 3>>;
   %template(UInt4Vector) vector<array<uint32_t, 4>>;
   %template(FloatVector) vector<float>;
   %template(Float2Vector) vector<array<float, 2>>;
   %template(Float3Vector) vector<array<float, 3>>;
   %template(Float4Vector) vector<array<float, 4>>;

   %template(TetVector) vector<UMeshLoader::Tet>;
   %template(PyrVector) vector<UMeshLoader::Pyr>;
   %template(WedgeVector) vector<UMeshLoader::Wedge>;
   %template(HexVector) vector<UMeshLoader::Hex>;
};

%apply bool& INOUT { bool& };

%include "./UMesh.h"
